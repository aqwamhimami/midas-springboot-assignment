package com.midas.test.controller;

import com.midas.test.entity.TutorialDTO;
import com.midas.test.entity.TutorialEntity;
import com.midas.test.service.TutorialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping(path = "api/tutorials")
public class TutorialController {

    @Autowired
    private TutorialService tutorialService;

    @PostMapping(path = "")
    public TutorialEntity createTutorial(@RequestBody TutorialDTO tutorialDTO) {
        try{
            return tutorialService.createTutorial(tutorialDTO);
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

    @GetMapping(path = "")
    public List<TutorialEntity> getAllTutorial() {
        try{
            return tutorialService.getAllTutorial();
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

    @GetMapping(path = "/{id}")
    public TutorialEntity getTutorialById(@PathVariable("id") int id) {
        try{
            return tutorialService.getTutorialById(id);
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

    @PutMapping(path = "/{id}")
    public TutorialEntity updateTutorialById(@PathVariable("id")int id, @RequestBody TutorialDTO tutorialDTO) {
        try{
            tutorialDTO.setId(String.valueOf(id));
            return tutorialService.updateTutorialById(tutorialDTO);
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

    @DeleteMapping(path = "/{id}")
    public String deleteTutorialById(@PathVariable("id")int id) {
        try{
            tutorialService.deleteTutorialById(id);
            return "200";
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

    @DeleteMapping(path = "")
    public String deleteAllTutorial() {
        try{
            tutorialService.deleteAllTutorial();
            return "200";
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

    @GetMapping(path = "/published")
    public List<TutorialEntity> getAllPublishedTutorial() {
        try{
            return tutorialService.getAllPublishedTutorial();
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }


    @GetMapping(path = "/title")
    public List<TutorialEntity> getTutorialByTitle(@RequestParam String title) {
        try{
            return tutorialService.getTutorialByTitle(title);
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }
}
