package com.midas.test.service;

import com.midas.test.entity.TutorialDTO;
import com.midas.test.entity.TutorialEntity;
import com.midas.test.repository.TutorialRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TutorialServiceImpl implements TutorialService{

    @Autowired
    private TutorialRepository tutorialRepository;

    @Override
    public TutorialEntity createTutorial(TutorialDTO tutorialDTO) {
        try {
            TutorialEntity tutorialEntity = new TutorialEntity();

            tutorialEntity.setDescription(tutorialDTO.getDescription());
            tutorialEntity.setPublished(tutorialDTO.isPublished());
            tutorialEntity.setTitle(tutorialDTO.getTitle());

            tutorialEntity = tutorialRepository.save(tutorialEntity);

            return tutorialEntity;
        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }

    }

    @Override
    public List<TutorialEntity> getAllTutorial() {
        try {
            return tutorialRepository.findAll();
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public TutorialEntity getTutorialById(int id) {
        try {
            return tutorialRepository.findById(id);
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public TutorialEntity updateTutorialById(TutorialDTO tutorialDTO) {
        try {
            TutorialEntity tutorialEntity = tutorialRepository.findById(Integer.parseInt(tutorialDTO.getId()));

            if (StringUtils.isNotBlank(tutorialDTO.getDescription()))
            tutorialEntity.setDescription(tutorialDTO.getDescription());

            tutorialEntity.setPublished(tutorialDTO.isPublished());

            if (StringUtils.isNotBlank(tutorialDTO.getTitle()))
            tutorialEntity.setTitle(tutorialDTO.getTitle());

            tutorialRepository.save(tutorialEntity);

            return tutorialEntity;
        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void deleteTutorialById(int id) {
        try {
            tutorialRepository.deleteById(id);
        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void deleteAllTutorial() {
        try {
            tutorialRepository.deleteAll();
        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public List<TutorialEntity> getAllPublishedTutorial() {
        try {
            return tutorialRepository.findPublishedTutorial();
        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public List<TutorialEntity> getTutorialByTitle(String title) {
        try {
            return tutorialRepository.findTutorialByTitle(title);
        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }
}
