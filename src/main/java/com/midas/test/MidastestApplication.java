package com.midas.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MidastestApplication {

    public static void main(String[] args) {
        SpringApplication.run(MidastestApplication.class, args);
    }

}
