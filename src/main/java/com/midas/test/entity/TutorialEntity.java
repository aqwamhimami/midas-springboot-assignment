package com.midas.test.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tutorials")
public class TutorialEntity {

    @Id
    @GeneratedValue
    private int id;
    private String description;
    private boolean published;
    private String title;
}
