package com.midas.test.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TutorialDTO {
    private String id;
    private String description;
    private boolean published;
    private String title;
}
