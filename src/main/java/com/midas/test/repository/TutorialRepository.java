package com.midas.test.repository;

import com.midas.test.entity.TutorialEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TutorialRepository extends JpaRepository<TutorialEntity, Integer> {

    public TutorialEntity findById(int id);

    @Query(value = "SELECT * FROM tutorials t WHERE t.published = true ", nativeQuery = true)
    public List<TutorialEntity> findPublishedTutorial();

    @Query(value = "SELECT * FROM tutorials t WHERE t.title LIKE %:title% ", nativeQuery = true)
    public List<TutorialEntity> findTutorialByTitle(@Param("title") String title);

}
